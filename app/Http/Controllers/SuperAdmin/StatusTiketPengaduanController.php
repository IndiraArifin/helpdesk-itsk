<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Models\Tiket;
use Illuminate\Http\Request;

class StatusTiketPengaduanController extends Controller
{
    public function index()
    {
        $jumlahBelumDiproses = Tiket::where('status_tiket', 'Belum Diproses')->count();
        $jumlahSedangDiproses = Tiket::where('status_tiket', 'Sedang Diproses')->count();
        $jumlahSelesai = Tiket::where('status_tiket', 'Selesai')->count();
        // Mengambil data kolom status_tiket
        $statuses = Tiket::pluck('status_tiket')->unique();

        // Menghitung jumlah data berdasarkan status_tiket
        $jumlahStatus = [];
        foreach ($statuses as $status) {
            $jumlahStatus[$status] = Tiket::where('status_tiket', $status)->count();
        }

        return view('superadmin.status_tiket', [
            'jumlahBelumDiproses' => $jumlahBelumDiproses,
            'jumlahSedangDiproses' => $jumlahSedangDiproses,
            'jumlahSelesai' => $jumlahSelesai,
            'jumlahStatus' => $jumlahStatus,
        ]);
    }
}
