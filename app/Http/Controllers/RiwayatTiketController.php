<?php

namespace App\Http\Controllers;

use App\Models\RiwayatTiket;
use App\Models\Tiket;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RiwayatTiketController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $kode = $request->kode;

        $tiket = Tiket::where('no_tiket', $kode)->first();
        if ($tiket) {
            if ($tiket->estimasi_selesai < now()) {
                $tiket->note = '<p style="color:red">Note: Estimasi telah melewati batas yang
                ditentukan.</p>';
            } else {
                $tiket->note = '<p style="color:green">Note: Estimasi masih belum melewati batas yang
                ditentukan.</p>';
            }
            $tiket->estimasi_selesai = Carbon::parse($tiket->estimasi_selesai)->translatedFormat('l, d F Y');
            $riwayatTiket = RiwayatTiket::where('id_tiket', $tiket->id)->get()->map(function ($riwayat) {
                $riwayat->waktu_riwayat = Carbon::parse($riwayat->waktu_riwayat)->translatedFormat('l, d F Y H:i T');
                return $riwayat;
            });
            $count = $riwayatTiket->count();
        } else {
            $riwayatTiket = collect();
            $count = 0;
        }

        return view('status', [
            'tiket' => $tiket,
            'riwayatTiket' => $riwayatTiket,
            'count' => $count,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
