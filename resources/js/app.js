import './bootstrap';

// Added: Actual Bootstrap JavaScript dependency
import 'bootstrap';

// Added: Popper.js dependency for popover support in Bootstrap
import '@popperjs/core';

// Misalkan Anda ingin menambahkan favicon berdasarkan path dari asset
function addFavicon() {
    var link = document.createElement('link');
    link.type = 'image/x-icon';
    link.rel = 'shortcut icon';
    link.href = '../assets/images/icon.png'; // Sesuaikan dengan path yang benar dari direktori public
    
    document.getElementsByTagName('head')[0].appendChild(link);
}

// Panggil fungsi addFavicon untuk menambahkan favicon
addFavicon();
